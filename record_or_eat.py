from Xlib import X, XK, display
import Xlib
import sys, time

from threading import Thread

def recordKeyboard(OnKey):
  record_dpy = display.Display()

  if not record_dpy.has_extension("RECORD"):
    print("RECORD extension not found", file=sys.stderr)
    sys.exit(1)

  ctx = record_dpy.record_create_context(
      0,
      [Xlib.ext.record.AllClients],
      [{
        'core_requests': (0, 0),
        'core_replies': (0, 0),
        'ext_requests': (0, 0, 0, 0),
        'ext_replies': (0, 0, 0, 0),
        'delivered_events': (0, 0),
        'device_events': (X.KeyPress,X.KeyRelease),
        'errors': (0, 0),
        'client_started': False,
        'client_died': False,
      }])

  def lookupKeysym(keysym):
      for name in dir(XK):
          if name.startswith("XK_") and getattr(XK, name) == keysym:
              return name.lstrip("XK_")
      return "[{}]".format(keysym)

  def processEvents(reply):
    if reply.category != Xlib.ext.record.FromServer or reply.client_swapped: return
    if (not reply.data) or (reply.data[0] < 2): return

    data = reply.data
    while len(data):
      event, data = Xlib.protocol.rq.EventField(None).parse_binary_value(data, record_dpy.display, None, None)
      if event.type == X.KeyPress or event.type == X.KeyRelease:
        keysym = record_dpy.keycode_to_keysym(event.detail, 0)
        name = lookupKeysym(keysym)
        OnKey(keysym, name, (event.type == X.KeyPress))
      else:
        #assert(0)
        #assert(event.type == X.ButtonPress)
        print(event, data)

  Thread(target=lambda:
    record_dpy.record_enable_context(ctx, processEvents)
      ).start()

class SuppressKeyboard:
  def __init__(self):
    self.disp = Xlib.display.Display()
    self.grabbed = False
  def grab(self, regrab = False):
    if not self.grabbed or regrab:
      #print("grab")
      #self.disp = Xlib.display.Display()

      self.disp.screen().root.grab_keyboard(X.KeyPressMask|X.KeyReleaseMask, X.GrabModeAsync, X.GrabModeAsync, X.CurrentTime)

      self.grabbed = True
  def ungrab(self):
    if self.grabbed:
      self.disp.ungrab_keyboard(X.CurrentTime)
      self.disp.flush()
      self.grabbed = False

class ControlMouse:
  def __init__(self):
    self.disp = Xlib.display.Display()

  def pos(self):
    p = self.disp.screen().root.query_pointer()
    return (p.root_x,p.root_y)

  def post(self, *args, **kwargs):
    Xlib.ext.xtest.fake_input(self.disp, *args, **kwargs)

  def move(self, dx, dy):
    x,y = self.pos()
    x += dx
    y += dy
    self.post(X.MotionNotify, x=round(x), y=round(y))
    self.disp.flush()




def OnKey(key, name, press):
  print(key, name, press)

if __name__ == "__main__":
  recordKeyboard(OnKey)
  sup = SuppressKeyboard()
  sup.grab()
  time.sleep(2)
  sup.ungrab()

# Keyboard mouse controller

### Installation
pip install -r requirements.txt

### Contents
  * keyboard_mouse_controller.py
    - Main script to control mouse with keyboard
    - Quit with Ctrl-q
    - Detailed controls in a comment at the top of keyboard_mouse_controller.py
  * aim.py
    - Simple game where you click circles spawning at random locations
    - Useful for testing and benchmarking keybindings, cursor speed, acceleration, etc.
  * plot_aim.txt
    - Plot score statistics from aim.py

### My setup
Gnome is configured to have Ctrl-Space run keyboard_mouse_controller.py

x = []
for line in open("aim.txt").read().split('\n'):
  if line[:1] == 'S':
    x.append(int(line.split(':')[1]))
import matplotlib.pyplot as plt
s = []
w = 10
a = 6
assert(w%2 == 0 and a%2 == 0)
off = (w-a)//2
for i in range(len(x)-w+1):
  p = sorted(x[i:i+w])[off:off+a]
  s.append(sum(p)/a)
plt.plot(x[off:])
plt.plot(s)
plt.ylim([sorted(x)[len(x)//10]-2, sorted(x)[len(x)*9//10]+2])
plt.show()

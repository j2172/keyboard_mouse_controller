import pygame, pygame.gfxdraw
from pygame.locals import *
import time, random

sw, sh = 1000, 800

def main():
  pygame.font.init()
  screen = pygame.display.set_mode((sw,sh))
  clock = pygame.time.Clock()
  pygame.display.set_caption("Aim trainer")

  pygame.event.set_grab(1)

  radius = 30
  moverad = 20
  minmove = 200
  hidden_misses = 20

  modes = ["speed", "hidden"]
  mode = "speed"
  NOCLICK = False

  global tx,ty,curr
  def replace():
    global tx,ty,curr
    curr = 0
    lx,ly = tx,ty
    while (tx-lx)**2+(ty-ly)**2 < minmove**2:
      tx = random.randint(radius*4, sw-radius*4)
      ty = random.randint(radius*4, sh-radius*4)


  global score, misses, finish, start_time, visipos
  def reset():
    global score, misses, finish, start_time, visipos, tx, ty
    score = 0
    misses = 0
    finish = 0
    start_time = time.time()
    pygame.mouse.set_visible(1)
    visipos = pygame.mouse.get_pos()
    tx,ty = visipos
    replace()

  reset()

  font = pygame.font.Font(None, 50)

  visipos = pygame.mouse.get_pos()

  while 1:
    for e in pygame.event.get():
      if e.type == QUIT: return
      elif e.type == KEYDOWN:
        if e.key == K_ESCAPE: return
        elif e.key == K_TAB:
          mode = modes[(modes.index(mode)+1)%len(modes)]
        elif e.key != K_SPACE: continue

        reset()

      elif e.type == MOUSEBUTTONDOWN and e.button == 1:
        x,y = pygame.mouse.get_pos() #e.pos

        if (x-tx)**2+(y-ty)**2 <= curr**2:
          score += 1
          replace()
        elif not finish:
          misses += 1
          start_time -= 0.1

        pygame.mouse.set_visible(1)
        visipos = pygame.mouse.get_pos()

    if mode == "hidden":
      x,y = pygame.mouse.get_pos()
      if (x-visipos[0])**2+(y-visipos[1])**2 > moverad**2 and misses < hidden_misses:
        pygame.mouse.set_visible(0)
    elif mode == "speed" and NOCLICK:
      x,y = pygame.mouse.get_pos()
      if (x-tx)**2+(y-ty)**2 <= curr**2:
        score += 1
        replace()

    screen.fill((0,0,0))
    for i,text in enumerate("Escape to quit\nTab to toggle mode\nSpace to restart".split("\n")):
      img = font.render(text, 1, (100,100,100), (0,0,0))
      screen.blit(img, (sw-img.get_width()-30, sh+50*(i-4)))


    def centertext(text, y):
      img = font.render(text, 1, (255,255,255), (0,0,0))
      screen.blit(img, ((sw-img.get_width())//2, y))

    centertext("Score: %d"%score, 30)

    if mode == "hidden":
      if misses < hidden_misses:
        centertext("Misses left: %d"%(hidden_misses-misses), 30+50)
      else:
        if finish == 0:
          open("aim.txt", "a").write("Hidden: %d\n"%score)
        finish = 1

    elif mode == "speed":
      left = start_time + 30 - time.time()
      if left > 0:
        centertext("Time: %d"%int(left+1-1e-6), 30+50)
      else:
        if finish == 0:
          open("aim.txt", "a").write("Score: %d\n"%score)
        finish = 1
        curr = 0

    else: assert(0)

    if not finish:
      curr = min(curr+5, radius)
      pygame.draw.circle(screen, (255,0,0), (tx,ty), curr)


    pygame.display.flip()
    clock.tick(60)

if __name__ == "__main__": main()

#!/usr/bin/env python3
"""
Controls:
  Mouse movement:
    ASDF
    Speed Modifiers Ctrl,Shift,Alt
  Mouse press
    - Left Space W, Shift for ungrabbed press
    - Middle E
    - Right R
  Scroll
    - Up K T
    - Down J G
    Speed Modifiers Ctrl, Shift, Alt, Z
  Keyboard mode
    - Enter I, Q, V
    - Exit Ctrl-space
  Single keystroke mode
    - Enter C
    - Exit after a single keystroke
  Quit
    - Ctrl-Q

Want:
  One handed?
  Redundant keys? 
  Global jumps?
  Marks?
  Special modes for special purposes?

Known problems:
  Repainting overlay takes ~40% cpu while moving
  Need shift to ungrab while using gnome stuff like settings and open skype
"""

import pyautogui as auto
from record_or_eat import recordKeyboard, SuppressKeyboard, ControlMouse
import sys, psutil, os
from time import time, sleep
from threading import Thread

auto.PAUSE = 0
auto.FAILSAFE = False

FPS = 60

class Controller:
  def __init__(self):
    self.dx, self.dy = 0, 0
    self.down = {}
    self.suppress = SuppressKeyboard()

    self.useoverlay = True
    if self.useoverlay:
      self.app = QApplication([])
      self.overlay = Overlay(self)

    self.exit = False
    self.suppress.grab()
    self.mouse = ControlMouse()
    self.print_next = 0
    self.move_time = -1
    self.regrab = -1

    self.mode = "normal"

  def modDown(self, key):
    assert(key in ['alt','shift','control'])
    return any((key.capitalize()+'_'+add in self.down) for add in 'LR')

  def OnKey(self, code, key, press):
    if key == 'semicolon': key = ';'
    if key == 'bracketleft': key = '['
    #print(key)

    if press:
      if self.print_next:
        self.print_next = 0

      if key in self.down:
        pass
        #print("Key down while down:", key)
      else:
        self.down[key] = time()
        if key in 'asdfjkl;':
          self.move_time = time()

      if self.mode == 'normal':

        speed = 1
        if self.modDown('alt'): speed *= 6
        if self.modDown('control') or self.modDown('shift') or 'z' in self.down: speed *= 3

        if key == 'p':
          self.print_next = 1
        elif key == 'space' or key == 'w' or key == 'u':
          if self.modDown('shift'):
            self.regrab = -1
            self.suppress.ungrab()
          auto.mouseDown(button='left')
        elif key == 'e' or key == 'i':
          auto.mouseDown(button='middle')
        elif key == 'r' or key == 'o':
          auto.mouseDown(button='right')
        elif key == 'h' or key == 'g' or key == 'Down':
          auto.scroll(-speed)
        elif key == 'y' or key == 't' or key == 'Up':
          auto.scroll(speed)
        elif key == 'q' or key == '[':
          if self.modDown('control'):
            self.quit()
        elif key == 'm' or key == 'c':
          self.regrab = -1
          self.suppress.ungrab()
          self.mode = 'single'

      elif self.mode == 'single':
        if not key in ['Shift_L','Alt_L','Control_L','Shift_R','Alt_R','Control_R']:
          self.regrab = time()+1
          self.mode = 'normal'
      elif self.mode == 'typing':
        if self.modDown('control') and key == 'space':
          self.regrab = time()+1
          self.mode = 'normal'

      else: assert(0)

    else:
      assert(not press)
      if not key in self.down:
        print("Key up while up:", key)
      else:
        self.updateMoves()
        del self.down[key]

      if self.mode == 'normal':
        if key == 'space' or key == 'w' or key == 'u':
          auto.mouseUp(button='left')
          self.regrab = time()+1
        elif key == 'e' or key == 'i':
          auto.mouseUp(button='middle')
        elif key == 'r' or key == 'o':
          auto.mouseUp(button='right')
        elif key == '[' or key == 'n' or key == 'v' or key == 'q':
          self.regrab = -1
          self.suppress.ungrab()
          self.mode = 'typing'


  def quit(self):
    self.exit = 1
    self.suppress.ungrab()
    exit()

  def updateMoves(self):
    if self.mode != 'normal': return

    now = time()

    #Failsafe, don't hold more than 10s
    if now-self.move_time > 10: return

    dx,dy = 0,0
    for d in 'asdfjkl;':
      if d in self.down:
        dt = now-self.down[d]
        self.down[d] = now

        speed = 1e3 * ((now-self.move_time) * 3 + 0.2)
        if self.modDown('alt'): speed *= 4
        if self.modDown('control') or self.modDown('shift'): speed /= 3

        if d in 'aj': dx -= dt*speed
        if d in 'sk': dy += dt*speed
        if d in 'dl': dy -= dt*speed
        if d in 'f;': dx += dt*speed

    self.dx += dx
    self.dy += dy

  def loop(self):
    last_time = time()
    while not self.exit:
      wait = 1/FPS+last_time-time()
      if wait > 0:
        sleep(wait)
      new_time = time()
      dt = new_time-last_time
      last_time = new_time

      if abs(1/dt-FPS) > 5:
        print('Warning, bad FPS', 1/dt, '/', FPS)

      #print(self.dx, self.dy)
      #print(self.down)

      self.updateMoves()
      if abs(self.dx) > 0.5 or abs(self.dy) > 0.5:
        ix, iy = round(self.dx), round(self.dy)
        self.dx -= ix
        self.dy -= iy
        self.mouse.move(ix, iy)

      if self.regrab != -1:
        self.suppress.grab(1)
        if time() > self.regrab:
          self.regrab = -1

      if self.useoverlay:
        self.app.processEvents()
        self.overlay.myUpdate()

    print("Finishing through self.exit")

  def alertErrors(self, f):
    def f_(*args):
      try:
        f(*args)
      except Exception as e:
        print()
        print('Keyboard mouse controller crashed')
        print(e)
        if len(sys.argv) == 2 and sys.argv[1] == "alert":
          Thread(target=lambda:
                auto.alert(text=e, title='Keyboard mouse controller crashed', button='OK', timeout=10000)
              ).start()
        self.quit()
    return f_



from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import Qt

class Overlay(QWidget):
  def __init__(self, par):
    QMainWindow.__init__(self)

    self.setWindowTitle("Keyboard mouse controller")

    hide = 1
    if hide:
      self.setAttribute(Qt.WA_TransparentForMouseEvents, True)
      self.setAttribute(Qt.WA_NoChildEventsForParent, True)
      self.setWindowFlags(Qt.Window|Qt.X11BypassWindowManagerHint|Qt.WindowStaysOnTopHint|Qt.FramelessWindowHint)
      self.setAttribute(Qt.WA_TranslucentBackground)
      #self.setAttribute(Qt.WA_ShowWithoutActivating);

    self.setGeometry(0,0,1920,1080)
    self.show()
    #self.showFullScreen()

    self.par = par
    self.lastpos = None
    self.lastmode = None

  def myUpdate(self):
    pos = self.mapFromGlobal(QtGui.QCursor.pos())
    if pos == self.lastpos and self.lastmode == self.par.mode: return
    self.lastpos = pos
    self.lastmode = self.par.mode
    self.update()

  def paintEvent(self, event):
    if not self.lastmode == 'normal': return
    #print("paint")

    pos = self.lastpos #self.mapFromGlobal(QtGui.QCursor.pos())
    x,y = pos.x(), pos.y()

    #painter.setRenderHint(QPainter.Antialiasing)
    #pen.setCapStyle(Qt.RoundCap)
    #painter.setBrush(QtCore.Qt.white)

    #painter.setRenderHint(QPainter.Antialiasing)
    #pen = QPen(Qt.red, 10)
    #pen.setCapStyle(Qt.RoundCap)
    #painter.setPen(pen)


    # Direction lines
    painter = QPainter()
    painter.begin(self)
    painter.setRenderHint(QPainter.Antialiasing)
    pen = QPen(Qt.cyan, 2)
    painter.setPen(pen)
    w,h = 1920,1080
    for x0 in [0,w//2,w]:
      for y0 in [0,h//2,h]:
        if x0 == w//2 and y0 == h//2: continue
        dx,dy = x-x0,y-y0
        l = (dx**2+dy**2)**.5
        f = min(l/2, 50)/(l+1e-9)
        dx *= f
        dy *= f
        painter.drawLine(x0, y0, round(x0+dx), round(y0+dy))
    painter.end()


    # Axis lines
    """
    painter = QPainter()
    painter.begin(self)
    pen = QPen(Qt.gray, 1)
    painter.setPen(pen)
    painter.drawLine(0, y, 1920, y)
    painter.drawLine(x, 0, x, 1080)
    painter.end()
    """


if __name__ == "__main__":
  mypid = os.getpid()
  running = [psutil.Process(pid).cmdline() for pid in psutil.pids() if pid != mypid]
  pythons = [i for i in running if 'python3' in i]
  thises = [i for i in pythons if len(i) >= 2 and sys.argv[0].split('/')[-1] == i[1].split('/')[-1]]
  if thises:
    print("Already running, not starting another one")
    exit(0)
  
  """
  app = QApplication([])
  overlay = Overlay()
  print("OK")
  while 1:
    overlay.update()
    app.processEvents()
    sleep(0.1)"""
  control = Controller()
  recordKeyboard(control.alertErrors(control.OnKey))
  control.loop()

